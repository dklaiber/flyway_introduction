IF COL_LENGTH(N'dbo.Users', N'phone_number') IS NULL
    BEGIN
        ALTER TABLE dbo.users
            ADD phone_number bigint
    END
GO

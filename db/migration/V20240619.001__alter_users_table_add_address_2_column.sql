IF COL_LENGTH(N'dbo.Users', N'address_2') IS NULL
    BEGIN
        ALTER TABLE dbo.users
            ADD address_2 bigint
    END
GO

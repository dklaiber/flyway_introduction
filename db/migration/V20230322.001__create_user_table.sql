IF NOT EXISTS(SELECT *
              FROM sys.objects
              WHERE object_id = OBJECT_ID(N'[dbo].[Users]')
                AND type in (N'U'))
CREATE TABLE users
(
    id    INT PRIMARY KEY,
    firstname VARCHAR(50),
    lastname  VARCHAR(50),
    email     VARCHAR(100),
    password  VARCHAR(50)
);


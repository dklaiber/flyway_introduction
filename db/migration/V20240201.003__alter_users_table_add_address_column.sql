IF COL_LENGTH(N'dbo.Users', N'address') IS NULL
    BEGIN
        ALTER TABLE dbo.users
            ADD address bigint
    END
GO

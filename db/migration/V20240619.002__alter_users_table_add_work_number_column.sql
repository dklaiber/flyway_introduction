IF COL_LENGTH(N'dbo.Users', N'work_number') IS NULL
    BEGIN
        ALTER TABLE dbo.users
            ADD work_number bigint
    END
GO

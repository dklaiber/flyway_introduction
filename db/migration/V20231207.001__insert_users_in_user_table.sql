IF NOT EXISTS(SELECT firstname
              FROM users
              WHERE firstname = 'John')
    INSERT INTO users (id, firstname, lastname, email, password)
    VALUES (3, 'Johnny', 'Dee', 'johndee@example.com', 'password');


UPDATE users
SET email = null
where id = 3;

UPDATE users
SET password = null
where id = 3;

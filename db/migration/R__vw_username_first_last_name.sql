IF OBJECT_ID('dbo.vw_usernames') IS NOT NULL
    DROP VIEW [dbo].[vw_usernames]
GO

CREATE VIEW vw_usernames AS
SELECT firstName, lastname, email
FROM users;

